package edu.towson.cis.cosc442.project1.monopoly;

// TODO: Auto-generated Javadoc
/**
 * The Interface IOwnable.
 */
public interface IOwnable {

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	String getName();

	/**
	 * Gets the owner.
	 *
	 * @return the owner
	 */
	Player getOwner();

	/**
	 * Sets the owner.
	 *
	 * @param owner the new owner
	 */
	void setOwner(Player owner);

}